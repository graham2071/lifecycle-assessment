# Lifecycle assessment <!-- omit in toc -->

Des ressources et connaissances sur les analyses de cycle de vie, et calculs d'impacts environnementaux et sociaux.

- [Entreprises](#entreprises)
  - [Analyses de Cycle de Vie (LCA)](#analyses-de-cycle-de-vie-lca)
  - [Impact carbone](#impact-carbone)
- [Logiciels](#logiciels)
  - [Logiciels d'ACV](#logiciels-dacv)
  - [Autres](#autres)
- [Resources](#resources)
  - [LCA databases](#lca-databases)
  - [Normes](#normes)
  - [LCA examples](#lca-examples)

## Entreprises

### Analyses de Cycle de Vie (LCA)

* [GreenDelta :de:](https://www.openlca.org/) propose **openlca** un logiciel open source permettant de modéliser les cycles de vie, calculer les impacts environementaux, sociaux et économiques.
* [Sapiologie :fr:](https://www.sapiologie.com/) développe des outils numériques sur mesure permettant de quantifier et de simuler les impacts environnementaux, sociaux et économiques. Connection ERP. #ESS
* [Boavizta :fr:](https://www.boavizta.org/) est un groupe de travail qui propose de calculer les impacts environnementaux du numérique.
* [Trace for good :fr:](https://traceforgood.com/) propose un outil de calcul d'impact environnemental et social pour l'industrie textile.
* [Carbonfact :fr:](https://www.carbonfact.com/) propose un outil de calcul d'impact environnemental pour l'industrie de la mode.
* [altermaker :fr:](https://altermaker.com/) propose le logiciel Ecodesign Studio pour calcul d'impact environnemental et eco design.
* [ecochain :netherlands:](https://ecochain.com/) propose des logiciels de calcul d'analyse de cycle de vie.
* [sphera :us:](https://sphera.com/?lang=fr) améliore la performance et la gestion des risques environnementaux, sociaux et de gouvernance (ESG) grâce à des logiciels intégrés, des données et des solutions de conseil. Sphera est une société du portefeuille de Blackstone, une société américaine d’investissement en actifs alternatifs.
* [One Click LCA :finland:](https://www.oneclicklca.com/), ex-Bionova Ltd, propose un logiciel de calcul d'ACV est EPD (Environmental Product Declarations).
* [PRé Sustainability :netherlands:](https://pre-sustainability.com/) développe le logiciel de calcul d'ACV SimaPro. L'entreprise travaille avec des partenaires locaux pour la revente et le support.
* [sami :fr:](https://www.sami.eco/) est une solution climat pour les entreprises, avec un logiciel de comptabilité carbone et un cabinet de conseil.
* [Makersite :de:](https://makersite.io/) Product Lifecycle Intelligence software brings together your cost, environment, compliance, and risk data in one place

### Impact carbone

* [orki :fr:](https://orki.green/) permet de calculer les émissions carbone (GHG Protocol, Méthode Bilan Carbone, ISO). Connexion ERP.
* [sweep :fr:](https://www.sweep.net/) permet de mesurer les empreintes carbone des entreprises.
* [Watershed :us:](https://watershed.com/) mesures les émissions et les gaspillages d'eau en vue de les réduire.
  * [Levée de fond de $100 Million 2024-02](https://www.esgtoday.com/corporate-climate-platform-watershed-raises-100-million/)

## Logiciels

### Logiciels d'ACV

| Software                                       | OS          | Licence    |
| ---------------------------------------------- | ----------- | ---------- |
| [OpenLCA](https://www.openlca.org/)            | Local       | OSS        |
| [Ecochain Mobius](https://ecochain.com/mobius) | Cloud-based | Commercial |
| [Ecochain Helix](https://ecochain.com/helix)   | Cloud-based | Commercial |
| [GaBi](https://www.gabi-software.com)          | Local       | Commercial |
| [Oneclicklca](https://www.oneclicklca.com/)    | Cloud-Based | Commercial |
| [SimaPro](https://network.simapro.com/evea/)   | Windows     | Commercial |

### Autres

* [ecobalyse](https://ecobalyse.beta.gouv.fr/) est un outil en ligne (+API) de l'Ademe qui permet le calcul de l'impact environnemental des produits textile et alimentaire. Dans le but de couvrir l'[affichage environnemental](https://www.sami.eco/blog/affichage-environnemental).

## Resources

### LCA databases

* [ecoinvent](https://ecoinvent.org/): référence mondiale de données pour les ACV.
* [agribalyse](https://agribalyse.ademe.fr/): bases de données gratuites, française, sur l'alimentation.
* [Psilca](https://psilca.net/): BD avec données sociales.

### Normes

* [ISO 14040:2006](https://www.iso.org/fr/standard/37456.html) spécifie les principes et le cadre applicables à la réalisation d'analyses du cycle de vie.
* [ISO 14044:2006](https://www.iso.org/fr/standard/38498.html) spécifie les exigences et fournit les lignes directrices pour la réalisation d'analyses du cycle de vie (ACV).
* [Corporate Sustainability Reporting Directive (CSRD)](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A52021PC0189).  
  Voir aussi:
  * le concept de [double matérialité](https://www.sami.eco/blog/double-materialite-csrd).
* [European Sustainability Reporting Standards (ESRS)](https://ec.europa.eu/finance/docs/level-2-measures/csrd-delegated-act-2023-5303-annex-1_en.pdf).  
  Voir aussi:
  * [Checklist des ESRS](https://docs.google.com/spreadsheets/d/1LJBwMu_N-0gZV_9fVOOJPWXv2hCVeWBddOqxHtacRxU/edit#gid=2114318583)

### LCA examples

* [Integrated Life-cycle Assessment
of Electricity Sources](https://unece.org/sites/default/files/2022-04/LCA_3_FINAL%20March%202022.pdf)
